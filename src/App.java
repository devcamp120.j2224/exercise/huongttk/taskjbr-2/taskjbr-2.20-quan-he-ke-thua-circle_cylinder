import com.devcamp.jbr2s20.Circle;
import com.devcamp.jbr2s20.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
       
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2);
        Circle circle3 = new Circle(3, "green");

        System.out.println("Circle1: " + circle1);
        System.out.println("Circle2: " + circle2);
        System.out.println("Circle3: " + circle3);

        ///////
        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5, "green");
        Cylinder cylinder4 = new Cylinder(3.5, "green", 1.5);

        System.out.println("Cylinder1: " + cylinder1);
        System.out.println("Cylinder2: " + cylinder2);
        System.out.println("Cylinder3: " + cylinder3);
        System.out.println("Cylinder4: " + cylinder4);
    }
}

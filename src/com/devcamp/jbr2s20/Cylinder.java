package com.devcamp.jbr2s20;

public class Cylinder  extends Circle{
    double height = 0.0;

    

    public Cylinder() {
    }


    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, String color) {
        super(radius, color);
    }

    public Cylinder(double radius, String color, double height) {
        super(radius, color);
        this.height = height;
    }


    public double getHeight() {
        return height;
    }


    public void setHeight(double height) {
        this.height = height;
    }

     /////
   public double getVolume() {
    return getArea() * height;
}


    @Override
    public String toString() {
        return "Cylinder [height=" + height + ", radius = " + radius + ", color = " + color + ", The tich = " + this.getVolume() + "]";
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }

  

}

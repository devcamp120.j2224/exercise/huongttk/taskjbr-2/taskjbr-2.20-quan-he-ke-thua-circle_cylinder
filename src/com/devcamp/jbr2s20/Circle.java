package com.devcamp.jbr2s20;

public class Circle {
    double radius = 0.0;
    String color = "";
    
    public Circle() {
    }

    

    public Circle(double radius) {
        this.radius = radius;
    }



    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public double getArea () {
        return this.radius*this.radius*Math.PI;
    }

    @Override
    public String toString() {
        return "Circle [color = " + color + ", radius = " + radius + ", Area = "+ this.getArea() + "]";
    }
}
